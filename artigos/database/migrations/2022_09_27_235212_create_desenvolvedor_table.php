<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('desenvolvedor', function (Blueprint $table) {
            $table->id();
            $table->string('nome');
            $table->enum('classificacao', ['Trainee', 'Junior', 'Pleno', 'Senior'])->default('Trainee');
            $table->string('cidade');
            $table->integer('experiencia_ano');
            $table->enum('status', ['Ativo', 'Inativo'])->default('Ativo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('desenvolvedor');
    }
};
