<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ArtigoController;// List artigos
use App\Http\Controllers\DesenvolvedorController;// List desenvolvedor


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
// List artigos
Route::get('artigos', [ArtigoController::class, 'index']);

// List single artigo
Route::get('artigo/{id}', [ArtigoController::class, 'show']);

// Create new artigo
Route::post('artigo', [ArtigoController::class, 'store']);

// Update artigo
Route::put('artigo/{id}', [ArtigoController::class, 'update']);

// Delete artigo
Route::delete('artigo/{id}', [ArtigoController::class,'destroy']);

// List desenvolvedores
Route::get('desenvolvedores', [DesenvolvedorController::class, 'index']);

// List single desenvolvedor
Route::get('desenvolvedor/{id}', [DesenvolvedorController::class, 'show']);

// Create new desenvolvedor
Route::post('desenvolvedor', [DesenvolvedorController::class, 'store']);

// Update desenvolvedor
Route::put('desenvolvedor/{id}', [DesenvolvedorController::class, 'update']);

// Delete desenvolvedor
Route::delete('desenvolvedor/{id}', [DesenvolvedorController::class,'destroy']);
