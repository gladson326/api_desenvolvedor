<?php

namespace App\Http\Controllers;

use App\Models\Desenvolvedor as Desenvolvedor;
use App\Http\Resources\Desenvolvedor as DesenvolvedorResource;
use Illuminate\Http\Request;

class DesenvolvedorController extends Controller{

public function index(){
    $desenvolvedores = Desenvolvedor::paginate(15);
    return DesenvolvedorResource::collection($desenvolvedores);
  }

  public function show($id){
    $desenvolvedor = Desenvolvedor::findOrFail( $id );
    return new DesenvolvedorResource( $desenvolvedor );
  }

  public function store(Request $request){
    $desenvolvedor = new Desenvolvedor;
    $desenvolvedor->nome = $request->input('nome');
    $desenvolvedor->classificacao = $request->input('classificacao');
    $desenvolvedor->cidade = $request->input('cidade');
    $desenvolvedor->experiencia_ano = $request->input('experiencia_ano');
    $desenvolvedor->status = $request->input('status');

    if( $desenvolvedor->save() ){
      return new DesenvolvedorResource( $desenvolvedor );
    }
  }

   public function update(Request $request){
    $desenvolvedor = Desenvolvedor::findOrFail( $request->id );
    $desenvolvedor->nome = $request->input('nome');
    $desenvolvedor->classificacao = $request->input('classificacao');
    $desenvolvedor->cidade = $request->input('cidade');
    $desenvolvedor->experiencia_ano = $request->input('experiencia_ano');
    $desenvolvedor->status = $request->input('status');

    if( $desenvolvedor->save() ){
      return new DesenvolvedorResource( $desenvolvedor );
    }
  } 

  public function destroy($id){
    $desenvolvedor = Desenvolvedor::findOrFail( $id );
    if( $desenvolvedor->delete() ){
      return new DesenvolvedorResource( $desenvolvedor );
    }

  }
  
}
