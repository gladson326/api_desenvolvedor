<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Desenvolvedor extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
      		'id' => $this->id,
      		'nome' => $this->nome,
      		'classificacao' => $this->classificacao,
		    'cidade' => $this->cidade,
            'experiencia_ano' => $this->experiencia_ano,
            'status' => $this->status

	];
    }
}
